THE 
   _   __ 
  | | / / 
  | |/ /  
  |   <   
  | |\ \  
  |_| \_\ 

  COMMAND


   DESCRIPTION
       This command is used when you are in a situation in which you could really just use a 'k' for a sassy purpose 
   OPTIONS
     -SIZING
       -l   Shows a Bigger version of the 'k'
       -s   Shows a Smaller version of the 'k'
       -t   Shows a Tiny version of the 'k'
     -COLORING
       -r   Prints the k in red
       -b   Prints the k in blue
       -y   Prints the k in yellow
       -g   Prints the k in green
     -MISC
       -d   Prints the k with a period animation
       -h   Prints the description and a list of commands.
INSTALL
RUN THE INSTALLER
----OR----
Go to your home directory.
Edit the .bashrc file by:
	1. go to your Home directory
	2. run gedit .bashrc
Add the following path to the PATH variable in the .bashrc file:
	export PATH=$PATH:/path/to/your/k
	MAKE SURE YOU REPLACE THE PATH WITH THE PATH TO K.SH ON YOUR MACHINE
Save the file
Refresh the file:
	Source .bashrc
The k command is now accessible from all directories!